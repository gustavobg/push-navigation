// required <script src="//cdnjs.cloudflare.com/ajax/libs/dom4/1.4.5/dom4.js"></script>

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define('pushNavigation', factory(root, $));
    } else if (typeof exports === 'object') {
        module.pushNavigation = factory(root, $);
    } else {
        root.pushNavigation = factory(root, $);
    }
})(this, function (root, $) {

    'use strict';

    if (typeof ($) === 'undefined') {
        throw new Error('JQuery is required, please ensure it is loaded before loading this validation plug-in');
    }

    // Variables
    var settings = {},
        onToggle = null, // toggle event
        defaults = {
            classUniform: 'checker'
        },
        self = this,

    // Methods
        /**
         * A simple forEach() implementation for Arrays, Objects and NodeLists
         * @@private
         * @@param {Array|Object|NodeList} collection Collection of items to iterate
         * @@param {Function} callback Callback function for each iteration
         * @@param {Array|Object|NodeList} scope Object/NodeList/Array that forEach is iterating over (aka `this`)
         */
        forEach = function (collection, callback, scope) {
            if (Object.prototype.toString.call(collection) === '[object Object]') {
                for (var prop in collection) {
                    if (Object.prototype.hasOwnProperty.call(collection, prop)) {
                        callback.call(scope, collection[prop], prop, collection);
                    }
                }
            } else {
                for (var i = 0, len = collection.length; i < len; i++) {
                    callback.call(scope, collection[i], i, collection);
                }
            }
        },
        /**
         * Merge defaults with user options
         * @@private
         * @@param {Object} defaults Default settings
         * @@param {Object} options User options
         * @@returns {Object} Merged values of defaults and options
         */
        extend = function (defaults, options) {
            var extended = {};
            forEach(defaults, function (value, prop) {
                extended[prop] = defaults[prop];
            });
            forEach(options, function (value, prop) {
                extended[prop] = options[prop];
            });
            return extended;
        },
        addClass = function (el, className) {
            if (el.classList)
                el.classList.add(className);
            else
                el.className += ' ' + className;
            return el;
        },
        wrapInner = function (el, before, after) {
            var wrapped = before + el.innerHTML + after;
            el.innerHTML = wrapped;
        },
        wrap = function (el, before, after) {
            el.outerHTML = before + el.outerHTML + after;
        },
        isChecked = function (el) {

        },
        di = function (id) {
            return document.getElementById(id);
        },
        parents = function (element, filter) {
            var parents = [];
            var parent = element;
            while (parent = parent.parentElement.closest(filter))
                parents.push(parent);
        },
        on = function(elSelector, eventName, selector, fn) {
            var element = typeof(elSelector) === 'string' ? document.querySelector(elSelector) : elSelector;

            element.addEventListener(eventName, function(event) {
                var possibleTargets = element.querySelectorAll(selector);
                var target = event.target;

                for (var i = 0, l = possibleTargets.length; i < l; i++) {
                    var el = target;
                    var p = possibleTargets[i];

                    while(el && el !== element) {
                        if (el === p) {
                            return fn.call(p, event);
                        }

                        el = el.parentNode;
                    }
                }
            });
        },
        // walk throught selector
        initialize = function (selector) {
            if (typeof (selector) === 'string') {
                forEach(elements, function (el) {
                    create(el);
                });
            } else if (typeof (selector) === 'object') {
                create(selector);
            };
        },
        create = function (el) {
            // navigate through 3th nested ul
            // append parent link title, concat with title
            // 3. ul.parent('li') <- concat parent('li')

            setupEvents(el);

            console.time('render');
            var menu = el,
                nested = menu.querySelectorAll('li > ul > li > ul > li > ul'),
                nestedLength = nested.length,
                detached = [],
                i = 0;
            console.time('render');

            //forEach(nested, function(ul) {
            for (i; i < nestedLength;) {
                var ul = nested[i],
                    prevLink = ul.previousElementSibling,
                    prevTitle = prevLink.querySelector('span').innerText,
                    mainLink = prevLink.parentElement.parentElement.previousElementSibling,
                    mainTitle = mainLink.querySelector('span').innerText,
                    mainLi = ul.parentElement.parentElement.parentElement,
                    mainUl = mainLi.parentElement,
                    nestedLi = document.createElement('li'),
                    a = document.createElement('a');
                a.href = "#";
                a.prepend(mainTitle + ' > ' + prevTitle);
                mainLi.classList.add('detached');
                nestedLi.append(ul.cloneNode(true));
                nestedLi.prepend(a);
                mainUl.append(nestedLi); // copy element to section

                i = i + 1;
                //detached.push(ul.cloneNode(true));
            }
            //});

            console.timeEnd('render');
            //if (el.tagName === 'INPUT' && (el.type === 'radio' || el.type === 'checkbox')) {
            //    var container = document.createElement('div');
            //    if (el.type === 'radio') {
            //        container.classList.add('vanilla-radio');
            //        el.before(container); // wraps
            //        container.append(el);
            //    } else {
            //        container.classList.add('vanilla-checkbox');
            //        el.before(container);
            //        container.append(el);
            //    }
            //    if (el.checked) {
            //        addClass(container, 'checked');
            //    }
            //    container.append(document.createElement('span').cloneNode(true)); // insert span to use content :before icon font
            //    setupEvents(el);
            //}
        },
        setupEvents = function (el) {
            var menu = el,
                menuToggle = $('.menu-toggle'),
                ul = $(el).find('ul').first();

            ul.on('click', 'a', function (e) {
                console.log(e.currentTarget, e);
                toggle(e.currentTarget.parentNode);

                $(el).trigger('pn.open', { d: 'Abriu' });

            });
            menuToggle.on('click', function (e) {
                toggleMenu(el);
            });

            //on(ul, 'click', 'a', function(e) {
            //    console.log(e.currentTarget, e);
            //    toggle(e.currentTarget.parentNode);
            //});

            //ul.addEventListener('click', function (e) {
            //    console.log(e.target, e);
            //    if (e.target.matches('a, a *')) {
            //        e.stopPropagation();
            //        toggle(e.target.parentNode);
            //    }
            //});
            //a.addEventListener('click', click);
            //input.addEventListener('change', change);
            //container.addEventListener('click', function (e) { click(e, input) });
        },
        change = function (e, input) {
            var input = e.target;
            console.log(input);
        },
        toggleMenu = function (el) {
            $(el).toggleClass('open');
        },
        toggle = function (li) {
            var allLis = li.parentNode.children,
                allLisLength = allLis.length,
                i = 0;

            if (li.classList.contains('open')) {
                li.classList.remove('open')
            } else {
                // remove all opened
                for (i; i < allLisLength;) {
                    allLis[i].classList.remove('open');
                    i = i + 1;
                }
                li.classList.add('open');
            }
        },
        //toggleElement = function (input) {
        //    var container = input.parentNode,
        //        value = false;
        //
        //    if (input.type === 'radio') {
        //        // remove checked markup
        //        if (input.getAttribute('name')) {
        //            forEach(document.getElementsByName(input.name), function (input) {
        //                input.removeAttribute('checked');
        //                input.parentNode.classList.remove('checked');
        //            });
        //        }
        //    }
        //    if (input.checked) {
        //        input.removeAttribute('checked');
        //        container.classList.remove('checked');
        //    } else {
        //        input.setAttribute('checked', 'checked');
        //        container.classList.add('checked');
        //        value = true;
        //    }
        //    // TODO: Trigger Events Vanilla [http://blog.garstasio.com/you-dont-need-jquery/events/]
        //    input.dispatchEvent(new CustomEvent('toggle', { bubbles: true, cancelable: true, detail: value }));
        //},
        /**
         * Initialize Plugin
         * @@public
         * @@param {Object} options User settings
         */
        exports = function (selector, options) {
            settings = extend(defaults, options || {}); // Merge user options with defaults
            initialize(selector, settings);
        };
    //exports.toggle = toggleElement;
    //exports.onToggle = onToggle;

    // Public APIs
    return exports;

});